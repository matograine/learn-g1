a place to catalog english-content for LibreMoney
---

https://libre-currency.org/|contains youtube videos /watch?v=(7LqdI7pALBE,plkpCm2vz9w,kmuzCWNFLtg,2nBPN-MKefA) and many other useful links... but is outdated by 2 years.

https://duniter.org/en/|excellent jumping-off point for english documentation.

https://en.trm.creationmonetaire.info/|probably needs readability work in 2nd half of trm.

https://git.duniter.org/spencer/learn-g1/-/blob/master/ my own learn-g1 repo to be "useful" for anglophone geeks around mid-January 2021.
