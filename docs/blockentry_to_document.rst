==========================
blockentry_to_document.py_
==========================

.. _blockentry_to_document.py: ../blockentry_to_document.py

Users of LibreMoney interact with the G1 blockchain via clients which publish signed documents to any available duniter node so that their interactions can be propagated through the rest of the network, eventually to be forged into a Block and added to the top of the blockchain.  Here, we will cover all DUBP (DUniter Blockchain Protocol) documents (Identity, Membership, Certification, Revocation, and Transaction) that clients (such as: Cesium, Silkaj, Sakia, and client libraries like DuniterPy) create on behalf of users. Specifically we will show that they are compressed into a compact-form when included in a Block; as well, we will show that each compact-form can be reverted into its orginal document so that its signature may be verified by all.

----------
Identities
----------
An Identity document is created when a user creates a member-account or when they convert a simple-wallet into a member-account.  It's required that they select a UniqueID within the system which is bound to their Public-Key.  It will be written in the blockchain if it is still valid, if the user has also published a Membership=IN document, and if they have received 5 available certifications.  This document acts like a contract: "I declare that this is my Public-Key and I want it to be bound to this UniqueID."

example from block #1437)

.. image:: ../media/blockentry_to_document_identity.png
::
	 
	$ ./blockentry_to_document.py 
	
	Enter the block-entry type (identity,joiner,active,leaver,certification,revoked,transaction), then hit <enter>
	identity
	
	Paste the quoted compact identity block-entry, then hit <enter>
	"Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P:Ac5hpamFgyoH+iyClr8kzDahQDrgE0KMpF2wOQ83FGOipLWYv7kOueF1V0tZk1QQ58XJJ/w+1JA0x+DfiTa4Cw==:179-000014A0348EF34A21D066EDC5F9D26D4604B272544A2EE6D316F8CFA0A38DCE:poka"
	
	Version: 10
	Type: Identity
	Currency: g1
	Issuer: Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P
	UniqueID: poka
	Timestamp: 179-000014A0348EF34A21D066EDC5F9D26D4604B272544A2EE6D316F8CFA0A38DCE
	Ac5hpamFgyoH+iyClr8kzDahQDrgE0KMpF2wOQ83FGOipLWYv7kOueF1V0tZk1QQ58XJJ/w+1JA0x+DfiTa4Cw==
	
	Document Signature Verified for Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P

-------
Joiners
-------
When a non-member is becoming a member, they are referred to as a joiner.  Their client will publish a Membership=IN document.  If it's their first time, it's published along with their Identity document.  This document acts like a contract: "My identity is this UniqueID and I hereby adhere to the G1 license and membership."

example from block #1437)

.. image:: ../media/blockentry_to_document_joiner.png
::
	
	$ ./blockentry_to_document.py 
	
	Enter the block-entry type (identity,joiner,active,leaver,certification,revoked,transaction), then hit <enter>
	joiner
	
	Paste the quoted compact joiner block-entry, then hit <enter>
	"Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P:dQDIsbEoCHzwP2JPJBHQhZgF/X9ux4EdPnuXIhyUbsFdGHE/ndemVsXHNfyf/Up6rJcoyjb3AqDtRYjaKaVLCQ==:179-000014A0348EF34A21D066EDC5F9D26D4604B272544A2EE6D316F8CFA0A38DCE:179-000014A0348EF34A21D066EDC5F9D26D4604B272544A2EE6D316F8CFA0A38DCE:poka"
	
	Version: 10
	Type: Membership
	Currency: g1
	Issuer: Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P
	Block: 179-000014A0348EF34A21D066EDC5F9D26D4604B272544A2EE6D316F8CFA0A38DCE
	Membership: IN
	UserID: poka
	CertTS: 179-000014A0348EF34A21D066EDC5F9D26D4604B272544A2EE6D316F8CFA0A38DCE
	dQDIsbEoCHzwP2JPJBHQhZgF/X9ux4EdPnuXIhyUbsFdGHE/ndemVsXHNfyf/Up6rJcoyjb3AqDtRYjaKaVLCQ==
	
	Document Signature Verified for Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P

-------
Actives
-------
Each year, existing members must republish new Membership=IN documents in order to remain members, they are referred to as actives.  This is simply reconfirming their previous contract, "My identity is still this UniqueID and I still adhere to the G1 license and membership."

example from block #17500)

.. image:: ../media/blockentry_to_document_active.png
::
	 
	$ ./blockentry_to_document.py 
	
	Enter the block-entry type (identity,joiner,active,leaver,certification,revoked,transaction), then hit <enter>
	active
	
	Paste the quoted compact active block-entry, then hit <enter>
	"13fn6X3XWVgshHTgS8beZMo9XiyScx6MB6yPsBB5ZBia:Y/UtNLHncwPajUXDYWKFArEZ9Jb5nz0s/vLsq7XfjvI7JHqwYfd6Hb6lOTir6z9N2W0eCyHV3GfccTluAmEIBQ==:17498-00000D2B0157644025DC3AA8CA4D89AD7AF77A79B25034FA3E06850D76494D24:11167-000004853A60356690E665EDB91739D66D8667E4B378E10E371DECFFDC399279:MelanieGraney"
	
	Version: 10
	Type: Membership
	Currency: g1
	Issuer: 13fn6X3XWVgshHTgS8beZMo9XiyScx6MB6yPsBB5ZBia
	Block: 17498-00000D2B0157644025DC3AA8CA4D89AD7AF77A79B25034FA3E06850D76494D24
	Membership: IN
	UserID: MelanieGraney
	CertTS: 11167-000004853A60356690E665EDB91739D66D8667E4B378E10E371DECFFDC399279
	Y/UtNLHncwPajUXDYWKFArEZ9Jb5nz0s/vLsq7XfjvI7JHqwYfd6Hb6lOTir6z9N2W0eCyHV3GfccTluAmEIBQ==
	
	Document Signature Verified for 13fn6X3XWVgshHTgS8beZMo9XiyScx6MB6yPsBB5ZBia

-------
Revoked
-------
If a member wants to permanently revoke their membership and identity (ie: because they've lost their secret-id/password and/or want to change these credentials, or change their UniqueID, or want to quit membership), they can publish a Revocation document and are referred to as revoked.  This document acts a contract: "My Identity is this UniqueID, I am currently a member, and I want to immediately and permenently revoke this identity AND membership."

example from block #33396)

.. image:: ../media/blockentry_to_document_revoked.png
::
	
	$ ./blockentry_to_document.py 
	
	Enter the block-entry type (identity,joiner,active,leaver,certification,revoked,transaction), then hit <enter>
	revoked
	
	Paste the quoted compact revoked block-entry, then hit <enter>
	"545kJHX6KbQLDkmfLtGmPdBj4mjSoRYjp2TaX43dqKfc:QYSj8i9SP0sWZ5FTX0RupgkgSZDTenLoyqNWSzYW8PEWzGehauCzzqXevr7OSLKBDNosFAdspvNuqi0XXLv3AA=="
	
	Version: 10
	Type: Revocation
	Currency: g1
	Issuer: 545kJHX6KbQLDkmfLtGmPdBj4mjSoRYjp2TaX43dqKfc
	IdtyUniqueID: Ella
	IdtyTimestamp: 24716-00000A82280FA948140EBD4F0EB48B8EE9901552B7421C4E057E7CD8F47C676D
	IdtySignature: Ul0JO2jnW3WWCYxFRHta5tt8DZ8tIGe+RkDmv8hxweaq1xy+s7mb3s0DA0Qw1X5ZJeBLzaYWM+gEzQoR6x6+Bg==
	QYSj8i9SP0sWZ5FTX0RupgkgSZDTenLoyqNWSzYW8PEWzGehauCzzqXevr7OSLKBDNosFAdspvNuqi0XXLv3AA==
	
	Document Signature Verified for 545kJHX6KbQLDkmfLtGmPdBj4mjSoRYjp2TaX43dqKfc

--------------
Certifications
--------------
When an existing member wants to vouch for another member, or for a future member, they publish a Certification document.  This document acts as a contract: "I am currently a member, I understand and follow the G1 license in good faith, I vouch that this other identity will do the same, and that I will be able to contact them within the following 2 years if a concern for the Web-of-Trust requires me to do so."

example from block #347)

.. image:: ../media/blockentry_to_document_certification.png
:: 
	
	$ ./blockentry_to_document.py 
	
	Enter the block-entry type (identity,joiner,active,leaver,certification,revoked,transaction), then hit <enter>
	certification
	
	Paste the quoted compact certification block-entry, then hit <enter>
	"36fz9dVQpnJ9USXvZRyEhVd6te6iR7N7vAYrAkvWZtvi:AmDcZSEB5MCt8GyZ1VMRt1sUwRH5D7HpXx8YKhMKZ1qa:345:6/UJnDMw2SyPaXHVV/Hyxe722bHkmOiOp6YiH2n1RyLb0AXw3KRMjD81EKgjfoegXiOXjG0SAgKYDpowByTnCw=="
	
	Version: 10
	Type: Certification
	Currency: g1
	Issuer: 36fz9dVQpnJ9USXvZRyEhVd6te6iR7N7vAYrAkvWZtvi
	IdtyIssuer: AmDcZSEB5MCt8GyZ1VMRt1sUwRH5D7HpXx8YKhMKZ1qa
	IdtyUniqueID: candide
	IdtyTimestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
	IdtySignature: SiFSo08MQ0736F3JZhUkoUBb3VTlkJRzLETmN7dbPyz4ShEpWTKABectsdCh8zbU3gN31t1KR5QQ8UPZgHRADg==
	CertTimestamp: 345-000007CFEAD4717F0D6DF580144D06F8210171840C5DDE23A69FEABA3732145B
	6/UJnDMw2SyPaXHVV/Hyxe722bHkmOiOp6YiH2n1RyLb0AXw3KRMjD81EKgjfoegXiOXjG0SAgKYDpowByTnCw==
	
	Document Signature Verified for 36fz9dVQpnJ9USXvZRyEhVd6te6iR7N7vAYrAkvWZtvi

------------
Transactions
------------
When any user wants to transfer G1 value from their own account to another, they publish a Transaction document. This document serves as a contract: "As the owner of these G1 coins, I authorize the following payment(s) to this/these account(s)."

example from block #52)

.. image:: ../media/blockentry_to_document_transactionA.png
::
	
	$ echo -e "Version: 10\nType: Block\nCurrency: g1\nNumber: 52\nPoWMin: 74\nTime: 1488990898\nMedianTime: 1488990117\nUnitBase: 0\nIssuer: 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ\nIssuersFrame: 6\nIssuersFrameVar: 0\nDifferentIssuersCount: 1\nPreviousHash: 00000FEDA61240DD125A26886FEB2E6995B52A94778C71224CAF8492FF257D47\nPreviousIssuer: 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ\nMembersCount: 59\nIdentities:\nJoiners:\nActives:\nLeavers:\nRevoked:\nExcluded:\nCertifications:\nTransactions:\nTX:10:1:1:1:2:1:0\n50-00001DAA4559FEDB8320D1040B0F22B631459F36F237A0D9BC1EB923C12A12E7\n2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ\n1000:0:D:2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ:1\n0:SIG(0)\n1:0:SIG(Com8rJukCozHZyFao6AheSsfDQdPApxQRnz7QYFf64mm)\n999:0:SIG(2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ)\nTEST\nfAH5Gor+8MtFzQZ++JaJO6U8JJ6+rkqKtPrRr/iufh3MYkoDGxmjzj6jCADQL+hkWBt8y8QzlgRkz0ixBcKHBw==\nInnerHash: 6B27ACDA51F416449E5A61FC69438F8974D11FC27EB7A992410C276FC0B9BA5F\nNonce: 10100000033688\n"
	Version: 10
	Type: Block
	Currency: g1
	Number: 52
	PoWMin: 74
	Time: 1488990898
	MedianTime: 1488990117
	UnitBase: 0
	Issuer: 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	IssuersFrame: 6
	IssuersFrameVar: 0
	DifferentIssuersCount: 1
	PreviousHash: 00000FEDA61240DD125A26886FEB2E6995B52A94778C71224CAF8492FF257D47
	PreviousIssuer: 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	MembersCount: 59
	Identities:
	Joiners:
	Actives:
	Leavers:
	Revoked:
	Excluded:
	Certifications:
	Transactions:
	TX:10:1:1:1:2:1:0
	50-00001DAA4559FEDB8320D1040B0F22B631459F36F237A0D9BC1EB923C12A12E7
	2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	1000:0:D:2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ:1
	0:SIG(0)
	1:0:SIG(Com8rJukCozHZyFao6AheSsfDQdPApxQRnz7QYFf64mm)
	999:0:SIG(2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ)
	TEST
	fAH5Gor+8MtFzQZ++JaJO6U8JJ6+rkqKtPrRr/iufh3MYkoDGxmjzj6jCADQL+hkWBt8y8QzlgRkz0ixBcKHBw==
	InnerHash: 6B27ACDA51F416449E5A61FC69438F8974D11FC27EB7A992410C276FC0B9BA5F
	Nonce: 10100000033688
 
.. image:: ../media/blockentry_to_document_transactionB.png
::
	
	$ ./blockentry_to_document.py 
	
	Enter the block-entry type (identity,joiner,active,leaver,certification,revoked,transaction), then hit <enter>
	transaction
	
	Paste the unquoted compact TX block-entry, then hit <Ctrl-D>
	TX:10:1:1:1:2:1:0
	50-00001DAA4559FEDB8320D1040B0F22B631459F36F237A0D9BC1EB923C12A12E7
	2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	1000:0:D:2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ:1
	0:SIG(0)
	1:0:SIG(Com8rJukCozHZyFao6AheSsfDQdPApxQRnz7QYFf64mm)
	999:0:SIG(2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ)
	TEST
	fAH5Gor+8MtFzQZ++JaJO6U8JJ6+rkqKtPrRr/iufh3MYkoDGxmjzj6jCADQL+hkWBt8y8QzlgRkz0ixBcKHBw==
	
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 50-00001DAA4559FEDB8320D1040B0F22B631459F36F237A0D9BC1EB923C12A12E7
	Locktime: 0
	Issuers:
	2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	Inputs:
	1000:0:D:2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ:1
	Unlocks:
	0:SIG(0)
	Outputs:
	1:0:SIG(Com8rJukCozHZyFao6AheSsfDQdPApxQRnz7QYFf64mm)
	999:0:SIG(2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ)
	Comment: TEST
	fAH5Gor+8MtFzQZ++JaJO6U8JJ6+rkqKtPrRr/iufh3MYkoDGxmjzj6jCADQL+hkWBt8y8QzlgRkz0ixBcKHBw==

	Document Signature Verified for 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ

------------------
GENESIS EXCEPTIONS
------------------
Included in each DUBP Document are timestamps of the form "BlockNum-BlockHash", known as a BlockUID, they can be used as a reference that the user must have created the document after this point in time.  When the original 59 member accounts were written to the Genesis block, those users needed some timesamp which did not yet exist, so they used 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855 (the hash of an empty string) for their signed Identity, Membership and Certification documents.

example from block 0)

.. image:: ../media/blockentry_to_document_certification_genesis.png
::
	
	$ ./blockentry_to_document.py 
	
	Enter the block-entry type (identity,joiner,active,leaver,certification,revoked,transaction), then hit <enter>
	certification
	
	Paste the quoted compact certification block-entry, then hit <enter>
	"D9D2zaJoWYWveii1JRYLVK3J4Z7ZH3QczoKrnQeiM6mx:2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ:0:MYWlBd2Hw3T/59BDz9HZECBuZ984C23F5lqUGluIUXsvXjsY4xKNqcN2x75s9rn++u4GEzZov6OznLZiHtbAAQ=="
	
	GENESIS EXCEPTION: using BlockUID from Identity for CertTimestamp
	
	Version: 10
	Type: Certification
	Currency: g1
	Issuer: D9D2zaJoWYWveii1JRYLVK3J4Z7ZH3QczoKrnQeiM6mx
	IdtyIssuer: 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	IdtyUniqueID: cgeek
	IdtyTimestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
	IdtySignature: GQSNirHpV6DGJ54wguZ/6qaQUor7EkWR3VfBkPcxmZtm0Ma07v4eTQYsDItIBFcUMeF5Yd4SBMnMNVyZ8WvsAQ==
	CertTimestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
	MYWlBd2Hw3T/59BDz9HZECBuZ984C23F5lqUGluIUXsvXjsY4xKNqcN2x75s9rn++u4GEzZov6OznLZiHtbAAQ==
	
	Document Signature Verified for D9D2zaJoWYWveii1JRYLVK3J4Z7ZH3QczoKrnQeiM6mx

--------------------------------------------------------------------------
Leavers: DEPRECATED as this functionality is made redundant by revocation.
--------------------------------------------------------------------------
If an existing member no longer wants to be a member, they can publish a Membership=OUT document, and are referred to as leavers.  This document serves as a contract: "My identity is this UniqueID but I no longer wish to be a member."

example from block #102093)

.. image:: ../media/blockentry_to_document_leaver.png
::
	
	$ ./blockentry_to_document.py 
	
	Enter the block-entry type (identity,joiner,active,leaver,certification,revoked,transaction), then hit <enter>
	leaver
	
	Paste the quoted compact leaver block-entry, then hit <enter>
	"7Pn2wLYhBzsC4kYCEeDMQyCLu2xpSbRUfe2StqLcJiaz:Oz/TPWv97LpQzH7Mt0KjUEW1s9AVcN9/l1iWBAXIg0SPxa+W4hI7Qkp+sxZ8IWgWwyH0lKiokWPJpYDnrF1NDw==:102091-0000002CC95CDE588677A49D057F73753A17D6A7D8018B94BC61F9AF4BA1CBF6:58127-0000030599AAA0978CE2E606C43944F9A9C3D9F01A98D65D9A72E4ECC2A1C3F9:brazouca"
	
	Version: 10
	Type: Membership
	Currency: g1
	Issuer: 7Pn2wLYhBzsC4kYCEeDMQyCLu2xpSbRUfe2StqLcJiaz
	Block: 102091-0000002CC95CDE588677A49D057F73753A17D6A7D8018B94BC61F9AF4BA1CBF6
	Membership: OUT
	UserID: brazouca
	CertTS: 58127-0000030599AAA0978CE2E606C43944F9A9C3D9F01A98D65D9A72E4ECC2A1C3F9
	Oz/TPWv97LpQzH7Mt0KjUEW1s9AVcN9/l1iWBAXIg0SPxa+W4hI7Qkp+sxZ8IWgWwyH0lKiokWPJpYDnrF1NDw==
	
	Document Signature Verified for 7Pn2wLYhBzsC4kYCEeDMQyCLu2xpSbRUfe2StqLcJiaz

