========================
unsigned_transaction.py_
========================

.. _unsigned_transaction.py: ../unsigned_transaction.py

Users of G1 libre money can send payments to others via Transaction document.

In this example, subsequently signed by learn-g1, published, and confirmed in Block #376477, a simple wallet account sends aproximately 2.5 DU each to simple wallet accounts for Alice and Bob.  When transfers of G1 are made, each transaction consumes 100% of its input sources.  In this particular case, since the sum of the outputs equals the inputs, there is no "change" output.

::
	
	$ ./unsigned_transaction.py 
	
	I will be requesting BMA results from a duniter node.
	
	Requesting http://localhost:20900/blockchain/current...
	...current height is 376472.
	
	Requesting http://localhost:20900/blochain/block/376466...
	...using 376466-0000000A906E99D3D1661C847FDECA779FEEFB01C0DE74172D4E102CFE20445D.
	
	Paste your base58 pubkey as Issuer, then hit <enter>
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	
	Requesting http://localhost:20900/tx/sources/3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC...
	...1 sources found.  Balance: 51.15 G1
	
	Specify the pay-to outputs below, one entry per line.
	Format each like: "Amount PubKey"; when done, hit <Ctrl-D>
	25.58 EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	25.57 8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	Total amount being spent: 51.15 G1

	Type anything to select specific sources, or nothing, then hit <enter>
	
	
	Enter a comment below, or nothing, then hit <enter>
	Welcome Alice and Bob to LibreMoney!
	
	
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 376466-0000000A906E99D3D1661C847FDECA779FEEFB01C0DE74172D4E102CFE20445D
	Locktime: 0
	Issuers:
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	Inputs:
	5115:0:T:A956972023A98799E18816FB76AF6C897BEFA29E708B3672A29878AE31C6EB2D:0
	Unlocks:
	0:SIG(0)
	Outputs:
	2558:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	2557:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	Comment: Welcome Alice and Bob to LibreMoney!

In this example, subsequently signed by Alice, published, and confirmed in Block #377031, simple-wallet account Alice is selectively choosing a single source, consuming 5DU, and sending half to simple-wallet account Bob.  Since her payment to Bob is NOT consuming 100% of the input source, a "change" output is included, which serves to send the remainder back to her account.  In most use-cases, we don't bother with sources or change outputs, instead, we rely on our client software to choose which sources will be consumed and to deal with our change; this example serves to illustrate that these functional details must be implemented by our chosen client software.

::
	
	$ ./unsigned_transaction.py 
	
	I will be requesting BMA results from a duniter node.
	
	Requesting http://localhost:20900/blockchain/current...
	...current height is 377029.
	
	Requesting http://localhost:20900/blochain/block/377023...
	...using 377023-0000002927C3D5EBFD3E46965430108A199293FECE0947166B705F8DB7805E1F.
	
	Paste your base58 pubkey as Issuer, then hit <enter>
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	
	Requesting http://localhost:20900/tx/sources/EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r...
	...2 sources found.  Balance: 76.73 G1
	
	Specify the pay-to outputs below, one entry per line.
	Format each like: "Amount PubKey"; when done, hit <Ctrl-D>
	25.58 8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	Total amount being spent: 25.58 G1
	
	Type anything to select specific sources, or nothing, then hit <enter>
	Anything?
	  0)      25.58 T 37A6417019B7D8D68CA005EDCC1F32E74D1244642A690F98D4DE7B001366C4F2:0
	  1)      51.15 T 957D1104B494D57F56C6DC02A194778D059B38CFBA42A59063CED990960A3B47:0
	
	Enter csv-list of indexes from above sources, then hit <enter>
	1
	
	Enter a comment below, or nothing, then hit <enter>
	example of source selection
	
	
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 377023-0000002927C3D5EBFD3E46965430108A199293FECE0947166B705F8DB7805E1F
	Locktime: 0
	Issuers:
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	Inputs:
	5115:0:T:957D1104B494D57F56C6DC02A194778D059B38CFBA42A59063CED990960A3B47:0
	Unlocks:
	0:SIG(0)
	Outputs:
	2558:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	2557:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	Comment: example of source selection


