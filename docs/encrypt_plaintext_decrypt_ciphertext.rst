================================================
encrypt_plaintext.py_ / decrypt_ciphertext.py_
================================================

.. _encrypt_plaintext.py: ../encrypt_plaintext.py
.. _decrypt_ciphertext.py: ../decrypt_ciphertext.py

**NOT part of the DUniter Blockchain Protocol**

Anyone who knows the public-key of a recipient, may encrypt a plaintext message which is scrambled into ciphertext and effectively meaningless for all eves-droppers, so that the plaintext may be recovered only by the intended recipient; however in this form the recipient cannot know who the sender of the message is.  This can be combined with a signature before encryption, in which case only the recipient can verify the sender, or after encryption if the sender desires that anyone in public can verify that they sent a message, without disclosing what was actually sent.

In the following example below, someone prepares an encrypted message for Bob using only his Public-key.

::
	
	$ ./encrypt_plaintext.py 
	
	Paste the recipient's base58 public-key below, then hit <enter>
	8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	
	Paste the plaintext message below, then hit <Ctrl-D>
	Hey Bob,
	
	Did you know that you have a secret admirer?
	
	
	PPeyRfEGvTjvF1KFz/SCgzWyjjjknr+PUq/Ri3BDwzz8tr5IdWeugFxULFI+zwEktNGvA6tJVk4h11eEmLsTgzxuW2RmZSlteBUWl2CuzFPCx9Z9CD3+NQxthtjiJtCDnxmn0yB0wg==
	
	Encrypted! The above base64 ciphertext may be decrypted by the owner of public-key: 8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	

In the following example, Bob having received the ciphertext, uses his Private-Key to decrypt it and learn the original message, but cannot verify who sent it.

::
	
	$ ./decrypt_ciphertext.py 
	
	Warning!
	Paste your base58 private-key or seed, then hit <enter>
	BOBS-PRIVATE-SECRET-SEED-HAS-BEEN-HIDDEN-HERE
	
	Paste the base64 ciphertext below, then hit <enter>
	PPeyRfEGvTjvF1KFz/SCgzWyjjjknr+PUq/Ri3BDwzz8tr5IdWeugFxULFI+zwEktNGvA6tJVk4h11eEmLsTgzxuW2RmZSlteBUWl2CuzFPCx9Z9CD3+NQxthtjiJtCDnxmn0yB0wg==
	
	
	Hey Bob,
	
	Did you know that you have a secret admirer?
	

