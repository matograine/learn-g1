====================================================
pkencrypt_plaintext.py_ / pkdecrypt_ciphertext.py_
====================================================

.. _pkencrypt_plaintext.py: ../pkencrypt_plaintext.py
.. _pkdecrypt_ciphertext.py: ../pkdecrypt_ciphertext.py

**Used in Cesium+ messages, but NOT part of the DUniter Blockchain Protocol**

A sender who knows the public-key of a recipient may encrypt a plaintext message, using their own Private-Key and the recipient's Public-key, which is scrambled into ciphertext and effectively meaningless for all eves-droppers, so that the plaintext may be recovered only by the intended recipient; In this form, once the recipient successfully decrypts the ciphertext to learn the message, they have also effectively authenticated the sender.

In the following example, Bob prepares an encrypted message for Alice using his Private-Key and her Public-Key.

::
	
	$ ./pkencrypt_plaintext.py 
	
	Warning!
	Paste your base58 private-key or seed, then hit <enter>
	BOBS-PRIVATE-SECRET-SEED-HAS-BEEN-HIDDEN-HERE
	
	Paste the recipient's base58 public-key below, then hit <enter>
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	
	Paste a 24-byte base64 nonce (or nothing) below, then hit <enter>
	
	
	Paste the plaintext message below, then hit <Ctrl-D>
	Dear Alice,
	
	Just wanted to let you know that you're my one and only.
	
	Truly yours,
	Bob
	
	z6qcsjHg9GRIDZUWOR2UMb2GjNnH5Cqm1IQXSALyRzBXn0zRikADgWK5kggnAS2LTE8A/l6pLQ06stxv5/8gJ2uRu8aTV9sCYr5h5j2HtMN2plBW69rVxRRpHC9cyeKInPgXrqbn2dIpjjHuvbDL8QE1AEjZP+EpWc1D0QTBm70=	
	
	Encrypted! The above base64 ciphertext may be decrypted, using your pubkey, by the owner of public-key: EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	

In the following example, Alice having received ciphertext from Bob, uses her Private-Key and his Public-Key to decrypt it, learning the original message, and authenticating that it came from Bob.

::
	
	$ ./pkdecrypt_ciphertext.py 
	
	Warning!
	Paste your base58 private-key or seed, then hit <enter>
	ALICES-PRIVATE-SECRET-SEED-HAS-BEEN-HIDDEN-HERE
	
	Paste the sender's base58 public-key below, then hit <enter>
	8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	
	Paste the base64 ciphertext below, then hit <enter>
	z6qcsjHg9GRIDZUWOR2UMb2GjNnH5Cqm1IQXSALyRzBXn0zRikADgWK5kggnAS2LTE8A/l6pLQ06stxv5/8gJ2uRu8aTV9sCYr5h5j2HtMN2plBW69rVxRRpHC9cyeKInPgXrqbn2dIpjjHuvbDL8QE1AEjZP+EpWc1D0QTBm70=	
	
	
	Dear Alice,
	
	Just wanted to let you know that you're my one and only.
	
	Truly yours,
	Bob
	

