================
hash_message.py_
================

.. _hash_message.py: ../hash_message.py

Throughout the G1 Blockchain, hashes of information are used as compact identifiers to precisely refer to other sets of information.

example from Block #1) having:
  * InnerHash: 64B858824682AB9D046F31DDACF7524FCD2501AEF269EE09E95EF2414DC9F7DB
  * bottom signature: zcF6EZF+i+U4KWsNjXGXbNNRRwO0BFjKl4EVsmpyv1qB4xzT+O2PRdkYoi8rH7/XX9stAhF+M/4pAA8YhkXKCQ==
  * Block hash: 0000238EB40E6A04B4E631FD4E2A757106D8C72C192BAE868D409D6359087C02
  * therefore BlockUID: 1-0000238EB40E6A04B4E631FD4E2A757106D8C72C192BAE868D409D6359087C02

.. image:: ../media/hash_messageA.png

.. image:: ../media/hash_messageB.png

The InnerHash is the hash of all lines of the Block beginning with "Version:" through the last transaction (ie: InnerHash, Nonce, and the bottom signature are not included).

::
	
	$ ./hash_message.py
	
	Paste the raw message below, (ie: InnerHash,Nonce + sig), then hit <Ctrl-D>
	Version: 10
	Type: Block
	Currency: g1
	Number: 1
	PoWMin: 70
	Time: 1488987233
	MedianTime: 1488987127
	UniversalDividend: 1000
	UnitBase: 0
	Issuer: 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	IssuersFrame: 1
	IssuersFrameVar: 5
	DifferentIssuersCount: 1
	PreviousHash: 000003D02B95D3296A4F06DBAC51775C4336A4DC09D0E958DC40033BE7E20F3D
	PreviousIssuer: 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	MembersCount: 59
	Identities:
	Joiners:
	Actives:
	Leavers:
	Revoked:
	Excluded:
	Certifications:
	Transactions:
	
	
	64B858824682AB9D046F31DDACF7524FCD2501AEF269EE09E95EF2414DC9F7DB

The Block hash is then composed of the lines containing the InnerHash, the Nonce, and the bottom signature.  Since the InnerHash is included in Block hash, it serves as a compact identifier for the entire block.

::
	
	$ ./hash_message.py
	
	Paste the raw message below, (ie: InnerHash,Nonce + sig), then hit <Ctrl-D>
	InnerHash: 64B858824682AB9D046F31DDACF7524FCD2501AEF269EE09E95EF2414DC9F7DB
	Nonce: 10100000025565
	zcF6EZF+i+U4KWsNjXGXbNNRRwO0BFjKl4EVsmpyv1qB4xzT+O2PRdkYoi8rH7/XX9stAhF+M/4pAA8YhkXKCQ==
	
	
	0000238EB40E6A04B4E631FD4E2A757106D8C72C192BAE868D409D6359087C02

Transaction documents are also hashed so that they can be identified when being spent in future transactions; referred to as input source identifiers.

example from Block #376477)

.. image:: ../media/hash_messageC.png

::
	
	$ ./hash_message.py 
	
	Paste the raw message below, (ie: InnerHash,Nonce + sig), then hit <Ctrl-D>
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 376466-0000000A906E99D3D1661C847FDECA779FEEFB01C0DE74172D4E102CFE20445D
	Locktime: 0
	Issuers:
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	Inputs:
	5115:0:T:A956972023A98799E18816FB76AF6C897BEFA29E708B3672A29878AE31C6EB2D:0
	Unlocks:
	0:SIG(0)
	Outputs:
	2558:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	2557:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	Comment: Welcome Alice and Bob to LibreMoney!
	K7fskEnThQK8X0mnHKiyBs1SjGBUl1SgtmsSSHG8/rLIrSUNUd48rC6h5d/BN+rj9NhWkZSeR/PQXgjkyQAtCQ==
	
	
	37A6417019B7D8D68CA005EDCC1F32E74D1244642A690F98D4DE7B001366C4F2
