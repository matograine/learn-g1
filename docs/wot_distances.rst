=================
wot_distances.py_
=================

.. _wot_distances.py: ../wot_distances.py

Traverse back through your local WoT.json, from any Identity

The WoT's distance rule demands that joining members, and those reconfirming their membership annually, must remain reachable by 80% of sentinels (members having 5 certs received and 5 certs sent) within 5 directed steps of the Web of Trust.

This tool steps backwards from any Identity in the WoT, to find as many sentinels as possible, then reports metrics useful in determining distances.

When script starts, it loads the local WoT.json and prints a summary of useful metrics; in this example, the number of certifications sent is NOT equal to the number of certs received (which makes no sense) and indicates that WoT.json is out of date, needs to be deleted, then re-created from scratch.
::
	
	./wot_distances.py 
	WoT.json loaded.
	WoT entries: 3870
	WoT certs sent: 31517, received: 31515
	WoT eldest: 181825, latest: 384289
	WoT members: 2878
	WoT sentinels: 1883

	Paste a base58 public-key or UniqueID, then hit <enter>
	cgeek
	
	Calculating distances backwards from 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ
	steps: 0, new sentinels: 1, total: 1
	steps: 1, new sentinels: 22, total: 23
	steps: 2, new sentinels: 245, total: 268
	steps: 3, new sentinels: 841, total: 1109
	steps: 4, new sentinels: 643, total: 1752
	steps: 5, new sentinels: 112, total: 1864
	steps: 6, new sentinels: 7, total: 1871
	cgeek is sentinel: True, member: True
	...uidblk: 0, eldest: 185771, latest: 378697
	visited: 2937
	non-members: 564
	non-sentinels: 1066
	isolated members: 505
	isolated sentinels: 12
	sentinels reached after 5 steps: 1863/1882 98.99%
	sentinels reached via cert sent: 1752/1883 93.04% quality: 1.16
