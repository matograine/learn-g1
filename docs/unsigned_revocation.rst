=======================
unsigned_revocation.py_
=======================

.. _unsigned_revocation.py: ../unsigned_revocation.py

A Revocation document is to be created, signed, and archived at the time a living person creates their Identity and requests Membership.  This can later be published in the event that the user loses their secret credentials, to permanently revoke their membership account so that they may re-start the process with a new simple-wallet.  This document can actually be created, signed, and published at any time as long as the user knows their secret credentials... they may do this in the event that someone else has learned their credentials, if they want to leave membership and retire their identity, or for whatever reason they so choose.

This example was not published; Alice is a fictitious character)

::
	
	$ ./unsigned_revocation.py 
	
	I will be requesting BMA results from a duniter node.
	
	Paste the base58 pubkey as Issuer, then hit <enter>
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	
	Requesting http://localhost:20900/wot/lookup/A2C6cVJnkkT2n4ivMPiLH2njQHeHSZcVf1cSTwZYScQ6...
	...results found.
	
	
	Version: 10
	Type: Revocation
	Currency: g1
	Issuer: EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	IdtyUniqueID: Alice
	IdtyTimestamp: 377247-0000000C6395EDA1E46B80891A59DE4F8D204BDE88435BC03ACECAF9C780DB43 
	IdtySignature: W9BCAt8RYhEd7qy61nocR2EGZFl8fPsj/HVbOH/4QRwadgtaV7IWmEB7B2d3ut6bEsQYJj8kFXtoJhIOzO0FCg==
	

