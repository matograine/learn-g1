=======================
unsigned_membership.py_
=======================

.. _unsigned_membership.py: ../unsigned_membership.py

A Membership document is signed and published along with an Identity in advance of membership to express that the living person adheres to the G1 licence and wishes to become a member.  It is also published annually to retain membership status; or can be published late, if excluded from membership, to regain their status.

The following example was not published; Alice is a fictitious character)

::
	
	$ ./unsigned_membership.py 
	
	I will be requesting BMA results from a duniter node.
	
	Paste the base58 pubkey as Issuer, then hit <enter>
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	
	Requesting http://localhost:20900/wot/lookup/EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r...
	...no results found.
	
	Requesting http://localhost:20900/blockchain/current...
	...current height is 377253.
	
	Requesting http://localhost:20900/blochain/block/377247...
	...using 377247-0000000C6395EDA1E46B80891A59DE4F8D204BDE88435BC03ACECAF9C780DB43
	
	Enter the unique pseudonym as UserID, then hit <enter>
	Alice
	
	
	Version: 10
	Type: Membership
	Currency: g1
	Issuer: EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	Block: 377247-0000000C6395EDA1E46B80891A59DE4F8D204BDE88435BC03ACECAF9C780DB43
	Membership: IN
	UserID: Alice
	CertTS: 377247-0000000C6395EDA1E46B80891A59DE4F8D204BDE88435BC03ACECAF9C780DB43
	

