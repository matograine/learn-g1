=========================
publish_dubp_document.py_
=========================

.. _publish_dubp_document.py: ../publish_dubp_document.py

A user's client, after creating a DUBP document and requesting the users secret credentials so that it can be signed, will publish the signed document to a duniter node so that it may be validated and propagated to the rest of the network.

example as confirmed in block #376477)

::
	
	$ ./publish_dubp_document.py

	I will be posting to a duniter node.
	
	Enter the DUBP Document type (Identity,Membership,Certification,Revocation,Transaction), then hit <enter>
	Transaction

	Paste your signed Transaction document, then hit <Ctrl-D>
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 376466-0000000A906E99D3D1661C847FDECA779FEEFB01C0DE74172D4E102CFE20445D
	Locktime: 0
	Issuers:
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	Inputs:
	5115:0:T:A956972023A98799E18816FB76AF6C897BEFA29E708B3672A29878AE31C6EB2D:0
	Unlocks:
	0:SIG(0)
	Outputs:
	2558:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	2557:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	Comment: Welcome Alice and Bob to LibreMoney!
	K7fskEnThQK8X0mnHKiyBs1SjGBUl1SgtmsSSHG8/rLIrSUNUd48rC6h5d/BN+rj9NhWkZSeR/PQXgjkyQAtCQ==
	
	Posting signed DUBP document as transaction to http://localhost:20900/tx/process...
	
	
	response: <Response [200]>
	response.json(): {'version': '10', 'currency': 'g1', 'issuers': ['3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC'], 'inputs': ['5115:0:T:A956972023A98799E18816FB76AF6C897BEFA29E708B3672A29878AE31C6EB2D:0'], 'outputs': ['2558:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)', '2557:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)'], 'unlocks': ['0:SIG(0)'], 'signatures': ['K7fskEnThQK8X0mnHKiyBs1SjGBUl1SgtmsSSHG8/rLIrSUNUd48rC6h5d/BN+rj9NhWkZSeR/PQXgjkyQAtCQ=='], 'comment': 'Welcome Alice and Bob to LibreMoney!', 'locktime': 0, 'hash': '37A6417019B7D8D68CA005EDCC1F32E74D1244642A690F98D4DE7B001366C4F2', 'written_block': 376466, 'raw': 'Version: 10\nType: Transaction\nCurrency: g1\nBlockstamp: 376466-0000000A906E99D3D1661C847FDECA779FEEFB01C0DE74172D4E102CFE20445D\nLocktime: 0\nIssuers:\n3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC\nInputs:\n5115:0:T:A956972023A98799E18816FB76AF6C897BEFA29E708B3672A29878AE31C6EB2D:0\nUnlocks:\n0:SIG(0)\nOutputs:\n2558:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)\n2557:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)\nComment: Welcome Alice and Bob to LibreMoney!\nK7fskEnThQK8X0mnHKiyBs1SjGBUl1SgtmsSSHG8/rLIrSUNUd48rC6h5d/BN+rj9NhWkZSeR/PQXgjkyQAtCQ==\n'}
