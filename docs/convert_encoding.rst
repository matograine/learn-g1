====================
convert_encoding.py_
====================

.. _convert_encoding.py: ../convert_encoding.py

A single set of data or information can be represented in many ways.  Information is useless without knowing how it is intended to be understood, so computers use encoding standards (to convert bytes of information into readable codes) for this reason.  Some encodings are more compact than others, and some are easier for humans to read/rewrite/transmit.  Information that has been previously encoded can be decoded by anyone as long as the encoding is known (or brute-forced).

The G1 LibreMoney project uses a few encoding standards:
  * DUBP Documents are encoded in ascii (a compatible subset of utf-8) that can easily be read by humans and which is used as a carrier of other encodings (ie: HEX, base58 and base64 values are regularly embeded in ascii or utf-8 documents, json query results, etc).
  * Public-Keys are represented in base58 everywhere.
  * Private-Keys are also represented in base58 (ie: PubSec/WIF/keyring.yml key-chains).
  * Private/Secret Seeds are represented in lowercase hex (ie: authfile key-chain).
  * Signatures are represented in base64.
  * Hashes are represented in uppercase HEX (to a computer, lowercase or uppercase hex are the same, but when a hash is included in a document that is later signed or hashed, this difference matters).

::
	
	$ ./convert_encoding.py
	
	Enter the input string below, then hit <enter>
	Welcome to LibreMoney! 
	
	Enter the input encoding (hex,HEX,base58,base64,ascii,utf-8,bytes), then hit <enter>
	ascii	
	
	Enter the output encoding (hex,HEX,base58,base64,ascii,utf-8,bytes), then hit <enter>
	hex
	
	Input converted from ascii to hex
	57656c636f6d6520746f204c696272654d6f6e657921
	
	$ ./convert_encoding.py
	
	Enter the input string below, then hit <enter>
	57656c636f6d6520746f204c696272654d6f6e657921
	
	Enter the input encoding (hex,HEX,base58,base64,ascii,utf-8,bytes), then hit <enter>
	hex
	
	Enter the output encoding (hex,HEX,base58,base64,ascii,utf-8,bytes), then hit <enter>
	base58
	
	Input converted from hex to base58
	Qin3aiajDfqeou7hbdmYDnMP6uzaMn
	
	$ ./convert_encoding.py
	
	Enter the input string below, then hit <enter>
	Qin3aiajDfqeou7hbdmYDnMP6uzaMn
	
	Enter the input encoding (hex,HEX,base58,base64,ascii,utf-8,bytes), then hit <enter>
	base58
	
	Enter the output encoding (hex,HEX,base58,base64,ascii,utf-8,bytes), then hit <enter>
	base64
	
	Input converted from base58 to base64
	V2VsY29tZSB0byBMaWJyZU1vbmV5IQ==
	
	$ ./convert_encoding.py
	
	Enter the input string below, then hit <enter>
	V2VsY29tZSB0byBMaWJyZU1vbmV5IQ==
	
	Enter the input encoding (hex,HEX,base58,base64,ascii,utf-8,bytes), then hit <enter>
	base64
	
	Enter the output encoding (hex,HEX,base58,base64,ascii,utf-8,bytes), then hit <enter>
	bytes
	
	Input converted from base64 to bytes 
	b'Welcome to LibreMoney!'

