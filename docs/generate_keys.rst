=================
generate_keys.py_
=================

.. _generate_keys.py: ../generate_keys.py

Users of G1 libre money are held to be responsible for keeping secret their Secret-Identifier and Passphrase.  This responsibility upon each of us cannot be over-emphasized. 

**ONLY you must ever know them, and you must ALWAYS be able to recall them.**

Failure to follow the above advice will render any expected utility absolutely useless.

Users interact with G1 libre money via authentication of their Secret-Identifier and Passphrase. Under the hood, these secrets are both passed (with other standard parameters) through a one-way hashing function known as 'scrypt' to create a 32-byte private/secret 'seed'.  This seed is then used to create the private-key (used by us when signing documents) and our public-key (used by others to identify us and to verify our signatures).  Some client softwares allow users to create and save a key-chain file, which can later be used non-interactively.  This is an advanced use case which requires the user have full-control and confidence of the device/hardware that their key-chain files are stored on.

In this repository, anytime that private-key material is required by one of its scripts, the program will display a blinking "Warning!" with white text on red background -- just above the prompt.  Please be careful whenever you see this. Also, learn to anticipate when key-material is going to be required, and carry this awareness with you while using other software.

::
	
	$ ./generate_keys.py 
	
	Warning!
	Enter scrypt salt (your secret-identifier), then hit <enter>
	SECRET-ID-HIDDEN
	
	Warning!
	Enter scrypt passphrase (your password), then hit <enter>
	THIS-PASSPHRASE-IS-HIDDEN
	
	Enter csv scrypt-parameters (default: 4096,16,1), then hit <enter>
	
	
	Warning!
	Specify a keychain type (authfile,pubsec,wif,ewif) or nothing, then hit <enter>
	
	creating keychain with: id=b'SECRET-ID-HIDDEN' and passwd=b'THIS-PASSPHRASE-IS-HIDDEN', N=4096, r=16, p=1
	seed (base58): THIS-BASE58-SEED-HAS-BEEN-HIDDEN
	privkey (base58): FOR-SECURITY-REASONS-THIS-BASE58-PRIVATE-KEY-HAS-BEEN-HIDDEN-FROM-THIS-DOCUMENTATION
	pubkey (base58): 3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	can sign and verify: True
