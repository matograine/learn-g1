=================================
CRYPTOGRAPHY BASICS - Terminology
=================================

Following are useful concepts -- building blocks we might strive to understand, in order to benefit everyday utility from available open source software based on cryptography.

**Cryptography:**
It's about securing free speech and expression in the digital world through security, confidentiality, and privacy.  Cryptography is an evolving field that offers security to an extent; however, perfect sucurity does not exist.  What is secure today may not always be secure in the future as technology evolves and mathematical discoveries are made.  In order to have realistic expectations, we should understand wht is being secured and the role we must play.

**One-way Function (aka: trap-door, asymmetric relationship):**
One-way functions produce a specific output from a specific input without feasibly reproducing the same input having only the output.  We can think of a camera as a one-way function.  We can take a photo of nature and share it with others. Everyone knows it is a picture of nature, but we cannot feasibly recreate nature having only the photo.

**Private Key (aka: Seed, Secret Credentials):**
It's a secret that ONLY the owner will ever know, and they must ALWAYS be able to recall it.  Assuming nobody else has access to this, each can trust that they are the only ones capable to speak or act, contractually, electronically, on their behalf.  The private key is just a number, but it can be represented as: a list of seed words, a secret-identifier/password, a precise string of seemingly random characters, and even as a QR-Code. Whichever manner the private key, or seed, or secret-id/password, is represented by the chosen software... it is crucial that ONLY the owner will ever know it and that they will ALWAYS be able to recall it. Make it strong, make multiple back-ups, then store them dispersed and secure.

**Public Key (aka: Address):**
The public key is like a public facing identity.  It is controlled only by the bearer of the private key.  It is a number, representing a coordinate, created by the private key. The public key is shared with others in order verify identifies in communications and transactions. A public key, or address, is represented as a nonmnemonic characters and is normally shared via QR-code.

**Relationship between Private and Public keys:**
The relationship between private and public keys is that a one-way function creates the public key from a private key. As the names suggests, the private key is kept secret while the public key can be shared.

**Information (aka: Message, Data):**
In the digital world, everything is information.  Whatever we say or do, digitally, requires and results in information.  Information is quantitative and qualitative, it has a size and specific value.

**Hash:**
A hash is a compact form of information to precisely identify and/or represent any other information (regardless of type, complexity or amount).  Its specialty is that it's a small identifier that can only be created by its original information.  Knowing only the hash we cannot learn any of the original. A hash can be used as a commitment, a declaration to others that we know something specific without sharing any details now, and later be able to prove that we knew them all along.  A hash is just a number. It can be represented as a nonmnemonic string of characters, shared via QR-code, but is most often used by software rather than the end user.

**Relationship between Information and its Hash:**
Information passes through a one-way hashing function to create a hash. Having only the hash, information, its size, and its value cannot be recreated.

**Signature:**
A signature is much like it’s conventional counterpart; however a cryptographic signature is more precise and verifiable.  A private key and a message is passed to a one-way signing function to create a signature.  A signature is a short message used with a public key and a copy of the information to verify the signer and the integrity of information. The information used in signatures is a often the hash of other data. A signature is often appended to the bottom of a document, represented as a nonmnemonic string of characters or QR-code, and is normally used by software rather than the end user.

**Relationship between Signer, Information, Signature, and the public.**
Only the owner of a private-key can sign information to create a signature. Anyone in public who knows the information and public-key of the signer can verify the signature, verifying specifically *who* signed *what*.

**CipherText (aka: encrypted message):**
Ciphertext is a form of information that has been scrambled through a one-way encryption function by the sender to prevent evesdropping.  There are a number of methods for doing this, with different trade-offs, but they are normally used by software rather than the end user.  Ciphertext is a nonmnemonic stream of characters which informs us nothing qualitative about the original. However its size is relative so we can learn something about the size of the original information.  Our software may or may not require our private-key when sending an encrypted message; in the prior case we're literally or effectively also signing the message.  Our software will require our private-key whenever decrypting an encrypted message, since it's meant only for us.

**Cryptographic Protocols:**
A Cryptographic protocol combines aforementioned concepts.  There are many, each with specific use-cases and trade-offs.  Protocols constantly evolving and can offer considerable utility when not misused.  Some are accessible to end users, others are more complex.
