======================
merge_unsigned_txs.py_
======================

.. _merge_unsigned_txs.py: ../merge_unsigned_txs.py

In the example below, learn-g1 is funding fictitious accounts for Alice and Bob, as well, welcoming a new one Mallory; Alice and Bob chip-in for the newcomer.  Previously, they have each created an unsigned Transaction document and are now merging them into one.  Afterwards, once each have carefully reviewed that their inputs haven't been changed and that their outputs meet or exceed their original, they would individually sign the merged Transaction document, appending their signatures in the same order as they appear in the Issuers list; then one of them would publish the signed merged Transaction.  An effectively similar, but not exact, version of this merged Transaction was confirmed in Block #377678.

::
	
	$ ./merge_unsigned_txs.py 
	
	I need unsigned Transaction documents pasted one at a time.
	
	Paste a Transaction document, or nothing, then hit <Ctrl-D>
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 377654-0000001965F4943427F30C18009C17CCB579D2969305EC90C3F717D4C6E50B27
	Locktime: 0
	Issuers:
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	Inputs:
	5115:0:T:2876B285EF17D6CCF4F5DD292694DD4821E66857B358E41062A41AE5AA633266:0
	Unlocks:
	0:SIG(0)
	Outputs:
	1000:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	1000:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	2000:0:SIG(w1pLhQVXp7kQuf4TFppE4wG6j8oV1bs7k4MooxxyGCZ)
	1115:0:SIG(3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC)
	Comment: Welcome Mallory! -learn-g1
	
	Paste a Transaction document, or nothing, then hit <Ctrl-D>
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 377655-00000020091835E6780ACA220DB53E60469CC5CC77927063C6FF52197158F370
	Locktime: 0
	Issuers:
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	Inputs:
	2558:0:T:37A6417019B7D8D68CA005EDCC1F32E74D1244642A690F98D4DE7B001366C4F2:0
	Unlocks:
	0:SIG(0)
	Outputs:
	1500:0:SIG(w1pLhQVXp7kQuf4TFppE4wG6j8oV1bs7k4MooxxyGCZ)
	1058:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	Comment: hi mal!
	
	Paste a Transaction document, or nothing, then hit <Ctrl-D>
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 377656-000000221BA0548C205DC4BAE34FBC1E61ACEFD5916FBE3B62BAF9CFBE4BAE76
	Locktime: 0
	Issuers:
	8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	Inputs:
	2557:0:T:37A6417019B7D8D68CA005EDCC1F32E74D1244642A690F98D4DE7B001366C4F2:1
	Unlocks:
	0:SIG(0)
	Outputs:
	1500:0:SIG(w1pLhQVXp7kQuf4TFppE4wG6j8oV1bs7k4MooxxyGCZ)
	1057:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	Comment:
	
	Paste a Transaction document, or nothing, then hit <Ctrl-D>
	
	
	Version: 10
	Type: Transaction
	Currency: g1
	Blockstamp: 377656-000000221BA0548C205DC4BAE34FBC1E61ACEFD5916FBE3B62BAF9CFBE4BAE76
	Locktime: 0
	Issuers:
	3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC
	8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX
	EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r
	Inputs:
	5115:0:T:2876B285EF17D6CCF4F5DD292694DD4821E66857B358E41062A41AE5AA633266:0
	2558:0:T:37A6417019B7D8D68CA005EDCC1F32E74D1244642A690F98D4DE7B001366C4F2:0
	2557:0:T:37A6417019B7D8D68CA005EDCC1F32E74D1244642A690F98D4DE7B001366C4F2:1
	Unlocks:
	0:SIG(0)
	1:SIG(2)
	2:SIG(1)
	Outputs:
	1115:0:SIG(3A9A6RNzZ7fNBsTmRwhJWCnKmWW13DkQ41M2MULZPpjC)
	2057:0:SIG(8txjWNFZhMJbKPijvnFybeksN1QpKaKJrM4jW8HhnFsX)
	2058:0:SIG(EVfy1VoZwbuN7L69kYiHxeosJLh5azkHV8G6TaSLy94r)
	5000:0:SIG(w1pLhQVXp7kQuf4TFppE4wG6j8oV1bs7k4MooxxyGCZ)
	Comment: Welcome Mallory! -learn-g1; hi mal!
	
