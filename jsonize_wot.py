#!/usr/bin/env python3
"""
json-ize WoT, so it can be inspected later w/o duniter requests. Is a hack.

... and remember kids, never roll your own crypto ;)
"""

import os
import time
import json

import requests

class InputError(Exception): pass

dos_response_content = b'{\n  "ucode": 1006,\n  "message": "This URI has reached its maximum usage quota. Please retry later."\n}'

def bma_request(url):
    answer = None
    for sleeptime in [1, 2, 4, 8, 16]:
        response = requests.get(url)
        if response.status_code == 200:
            answer = response.json()
            break
        else: 
            print('bma_request(%s): %d' % (url, response.status_code))
            if response.content == dos_response_content:
                time.sleep(20)
        time.sleep(sleeptime)
    return answer

def bma_height(node_uri):
    return bma_request('%s/blockchain/current' % node_uri)['number']
    
def bmawot_members(node_uri):
    answer = []
    results = bma_request(node_uri + '/wot/members')
    if results: 
        answer = [x['pubkey'] for x in results['results']]
    return answer

def bmawot_certifications(node_uri, of_by, pubkey):
    answer, url_fmt = None, '%s/wot/%s/%s'
    if of_by == 'certifiers-of': url = url_fmt % (node_uri, of_by, pubkey)
    elif of_by == 'certified-by': url = url_fmt % (node_uri, of_by, pubkey)
    else: raise InputError('%s not in (certifiers-of,certified-by)' % of_by)
    results = bma_request(url)
    if results:
        certs = [x for x in results['certifications']]
        blocks = [x['written']['number'] for x in certs if x['written']]
        answer = {
            'pubkey': results['pubkey'],
            'uid': results['uid'],
            'member': results['isMember'],
            'certs': list(set([x['pubkey'] for x in certs 
                 if x['written']])),
            'uidblk': int(results['sigDate'].split('-')[0]),
            'eldest': min(blocks) if blocks else None, 
            'latest': max(blocks) if blocks else None,
            }
    return answer

def bmawot_entry(node_uri, pubkey):
    answer = None
    received = bmawot_certifications(node_uri, 'certifiers-of', pubkey)
    sent = bmawot_certifications(node_uri, 'certified-by', pubkey)
    if received:
        eldest, latest = received['eldest'], received['latest']
        if sent:
            assert received['pubkey'] == sent['pubkey'] == pubkey
            assert received['uid'] == sent['uid']
            assert received['member'] == sent['member']
            assert received['uidblk'] == sent['uidblk']
            if eldest: 
                if sent['eldest'] and sent['eldest'] < eldest:
                    eldest = sent['eldest']
            else: 
                eldest = sent['eldest']
            if latest: 
                if sent['latest'] and sent['latest'] < latest:
                    latest = sent['latest']
            else: 
                latest = sent['latest']
        else:
            sent = {'certs': [], 'block': -1}
        answer = {
            'uid': received['uid'],
            'member': received['member'],
            'received': [x for x in received['certs']],
            'sent': [x for x in sent['certs']],
            'uidblk': received['uidblk'],
            'eldest': eldest,
            'latest': latest,
            }
    return answer

def bmawot_changes_since(node_uri, since_block):
    answer = []
    blocks = bma_request('%s/blockchain/with/certs' % node_uri
        )['result']['blocks']
    blocks += bma_request('%s/blockchain/with/joiners' % node_uri
        )['result']['blocks']
    blocks += bma_request('%s/blockchain/with/actives' % node_uri
        )['result']['blocks']
    blocks += bma_request('%s/blockchain/with/revoked' % node_uri
        )['result']['blocks']
    blocks += bma_request('%s/blockchain/with/excluded' % node_uri
        )['result']['blocks']
    blocks = sorted(list(set([x for x in blocks if x > since_block])))
    for block_num in blocks:
        block = bma_request('%s/blockchain/block/%d' % (node_uri, block_num)) 
        if 'certifications' in block:
            for certification in block['certifications']:
                from_, to = certification.split(':')[:2]
                answer.append({'block': block_num, 
                    'type': 'certification', 'from': from_, 'to': to})
        if 'joiners' in block:
            for joiner in block['joiners']:
                pubkey = joiner.split(':')[0]
                answer.append({'block': block_num,
                    'type': 'joiner', 'pubkey': pubkey})
        if 'actives' in block:
            for active in block['actives']:
                pubkey = active.split(':')[0]
                answer.append({'block': block_num,
                    'type': 'active', 'pubkey': pubkey})
        if 'revoked' in block:
            for revocation in block['revoked']:
                pubkey = revocation.split(':')[0]
                answer.append({'block': block_num,
                    'type': 'revoked', 'pubkey': pubkey})
        if 'excluded' in block:
            for pubkey in block['excluded']:
                answer.append({'block': block_num,
                    'type': 'excluded', 'pubkey': pubkey})
    return answer

sleeptime = 0
def bmawot_update(node_uri, wot, pubkeys=[], changes=[]):
    print('bmawot_update()...')
    try:
        for pubkey in pubkeys:
            if pubkey not in wot:
                wot_entry = bmawot_entry(node_uri, pubkey)
                if wot_entry:
                    print('...added %s %s' % (pubkey, wot_entry['uid']))
                    wot[pubkey] = wot_entry
                else:
                    print('unsuccessful for %s' % pubkey)
                time.sleep(sleeptime)

        for change in changes:
            if change['type'] == 'certification':
                to = wot[change['to']]
                if change['from'] not in to['received']:
                    to['received'].append(change['from'])
                from_ = wot[change['from']]
                if change['to'] not in from_['sent']:
                    from_['sent'].append(change['to'])
                entries = {change['to']: to, change['from']: from_}
                print('...cert from %s %s\n          to %s %s' % (
                   change['from'], from_['uid'], change['to'], to['uid']))
            else:
                entry = wot[change['pubkey']]
                if change['type'] in ('revoked', 'excluded'):
                    entry['member'] = False
                elif change['type'] in ('joiner', 'active'):
                    entry['member'] = True
                entries = {change['pubkey']: entry}
                print('...%s %s %s' % (change['type'], change['pubkey'], 
                    entry['uid']))
            for k,v in entries.items():
                entries[k]['latest'] = change['block']
            wot.update(entries)

    except KeyboardInterrupt: 
        print()
        wot_save(wot)
        raise
        
    print('...bmawot_update() done.')
    return wot

def wot_del_key_if_stale(wot, wot_height):
    print('wot_del_key_if_stale(%d)...' % wot_height)
    seconds_in_a_long_time = (23.5 * 30 * 24 * 60 * 60)
    blocks_in_a_long_time = (seconds_in_a_long_time / 300)
    stale_if = wot_height - blocks_in_a_long_time
    print('stale_if: %d' % stale_if)
    keys_to_del = [k for k,v in wot.items() if v['eldest'] <= stale_if]
    for k in keys_to_del:
        del wot[k]
        print('...del wot[%s]' % k)
    print('...wot_del_key_if_stale() done.')
    return wot

def wot_del_key_if_changes(wot, changes):
    print('wot_del_key_if_changes(%d)...' % len(changes))
    keys_to_del = []
    for change in changes:
        if change['type'] == 'certification':
            keys_to_del.append(change['to'])
            keys_to_del.append(change['from'])
        else:
            keys_to_del.append(change['pubkey'])
    for k in set(keys_to_del):
        if k in wot: 
           del wot[k]
           print('...del wot[%s]' % k)
    print('...wot_del_key_if_changes() done.')
    return wot

def wot_eldest(wot):
    return min([x['eldest'] for x in wot.values() if x['eldest']])

def wot_latest(wot):
    return max([x['latest'] for x in wot.values() if x['latest']])

sentinel_if = 5  # in reality: ceil(pow(num_members, 1/stepMax))
def wot_sentinels(wot):
    return [k for k,v in wot.items() if v['member'] 
        and len(v['received']) >= sentinel_if 
        and len(v['sent']) >= sentinel_if
        ]

def wot_members(wot):
    return [k for k,v in wot.items() if v['member']]

def wot_summary(wot):
    print('WoT entries: %d' % len(wot))
    print('WoT certs sent: %d, received: %d' % (
        sum([len(x['sent']) for x in wot.values()]),
        sum([len(x['received']) for x in wot.values()]),
         ))
    print('WoT eldest: %d, latest: %d' % (wot_eldest(wot), wot_latest(wot)))
    print('WoT members: %d' % len(wot_members(wot)))
    print('WoT sentinels: %d' % len(wot_sentinels(wot)))

def wot_extras(wot, pubkeys):
    extras = []
    for v in wot.values():
        extras += [x for x in v['received'] if x not in pubkeys]
        extras += [x for x in v['sent'] if x not in pubkeys]
    return set(extras)


def wot_load(filename='WoT.json'):
    def list2dict(alist):
        return {
            'uid': alist[0],
            'member': alist[1],
            'received': alist[2],
            'sent': alist[3],
            'uidblk': alist[4],
            'eldest': alist[5],
            'latest': alist[6],
            }
    wot = {}
    try:
        with open(filename, 'r') as fp:
            wot = json.load(fp)
        print('%s loaded.' % filename)
        wot = {k:list2dict(v) for k,v in wot.items()}
    except FileNotFoundError:
        print('%s not found.' % filename)
    return wot

def wot_save(wot, filename='WoT.json'):
    def dict2list(adict):
        return [
            adict['uid'],
            adict['member'],
            adict['received'],
            adict['sent'],
            adict['uidblk'],
            adict['eldest'],
            adict['latest'],
            ]
    wot = {k:dict2list(v) for k,v in wot.items()}
    with open(filename, 'w') as fp:
        json.dump(wot, fp)
    print('%s saved.' % filename)


if __name__ == '__main__':

    print('\nI may be requesting *many* BMA results from a duniter node.\n')

    node_uri = os.environ.get('DUNITER_BMA', None)
    default = 'http://127.0.0.1:20900'
    if not node_uri:
        print('\nEnter a duniter-bma URI (default: %s), then hit <enter>'
            % default)
        node_uri = input()
        if not node_uri: node_uri = default
    if 'localhost' in node_uri or '127.0.0.1' in node_uri:
        sleeptime = 0.1
    else:
        print('I will sleep 1s while populating to avoid DOSing %s' % node_uri)
        sleeptime = 1

    
    changes = []
    bma_height = bma_height(node_uri)
    expected = bmawot_members(node_uri)
    wot = wot_load()
    expected += wot_extras(wot, expected)
    # this is flawed, far from useful
    #if wot and len(wot) == len(expected): 
    #    changes = bmawot_changes_since(node_uri, wot_height(wot))
    # instead just delete changed keys... as long as wot seems 90% complete
    if wot and len(wot) > len(expected) *.9:
        changes = bmawot_changes_since(node_uri, wot_latest(wot))
    if changes: 
        wot = wot_del_key_if_changes(wot, changes)
        changes = []
    while [x for x in expected if x not in wot] or changes:
        print('WoT entries: %d, expected: %d' % (len(wot), len(expected)))
        wot = bmawot_update(node_uri, wot, pubkeys=expected, changes=changes)
        expected += wot_extras(wot, expected)
        changes = bmawot_changes_since(node_uri, wot_latest(wot))
    print('bma Height: %d, WoT entries: %d, expected: %d' % (
        bma_height, len(wot), len(expected)))
    wot_summary(wot)
    wot_save(wot)
