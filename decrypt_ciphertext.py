#!/usr/bin/env python3
"""
Decrypt "ciphertext" with recipient's "private-key" 

... and remember kids, never roll your own crypto ;)
"""

import sys
import base64

import base58
from libnacl import crypto_sign_ed25519_sk_to_curve25519 as private_sign2crypt
from libnacl.sign import Signer
from libnacl.public import SecretKey
from libnacl.sealed import SealedBox
from termcolor import colored

def decrypt(b58to_privkey_or_seed, b64ciphertext):
    seed = base58.b58decode(b58to_privkey_or_seed)[:32]
    signer = Signer(seed)
    sk = SecretKey(private_sign2crypt(signer.sk))
    box = SealedBox(sk)
    return box.decrypt(base64.b64decode(b64ciphertext)).decode('utf-8')
    
if __name__ == '__main__':

    warning = colored('Warning!', 'white', 'on_red', attrs=['blink', 'bold'])
    print("\n%s\nPaste your base58 private-key or seed, then hit <enter>"
        % warning) 
    privkey_or_seed = input()

    print("\nPaste the base64 ciphertext below, then hit <enter>")
    ciphertext = input()

    print("\n\n%s\n" % decrypt(privkey_or_seed, ciphertext))

