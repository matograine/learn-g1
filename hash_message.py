#!/usr/bin/env python3
"""
Get the uppercase hex sha256 hash output of a message

...and remember kids, never roll your own crypto ;)
"""

import sys
from hashlib import sha256

def hash(message): 
    return sha256(message).hexdigest().upper()

if __name__ == '__main__':

    print('\nPaste the raw message below, (ie: InnerHash,Nonce + sig), then hit <Ctrl-D>')
    msg = bytes(sys.stdin.read(), 'utf-8')

    print('\n\n%s' % hash(msg))
