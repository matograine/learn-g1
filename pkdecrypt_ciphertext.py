#!/usr/bin/env python3
"""
Decrypt "ciphertext" with a receiver "private-key" and sender "public-key"

... and remember kids, never roll your own crypto ;)
"""

import sys
import base64
import base58

from libnacl import crypto_sign_ed25519_sk_to_curve25519 as private_sign2crypt
from libnacl import crypto_sign_ed25519_pk_to_curve25519 as public_sign2crypt
from libnacl.sign import Signer, Verifier
from libnacl.public import SecretKey, PublicKey, Box
from termcolor import colored

def decrypt(b58to_privkey_or_seed, b58from_pubkey, b64ciphertext):
    seed = base58.b58decode(b58to_privkey_or_seed)[:32]
    signer = Signer(seed)
    sk = SecretKey(private_sign2crypt(signer.sk))
    vk = Verifier(base58.b58decode(b58from_pubkey).hex())
    pk = PublicKey(public_sign2crypt(vk.vk))
    box = Box(sk.sk, pk.pk)
    ciphertext = base64.b64decode(b64ciphertext)
    return box.decrypt(ciphertext).decode('utf-8')

if __name__ == '__main__':

    warning = colored('Warning!', 'white', 'on_red', attrs=['blink', 'bold'])
    print('\n%s\nPaste your base58 private-key or seed, then hit <enter>'
        % warning) 
    privkey_or_seed = input()

    print("\nPaste the sender's base58 public-key below, then hit <enter>")
    pubkey = input()

    print('\nPaste the base64 ciphertext below, then hit <enter>')
    ciphertext = input()

    plaintext = decrypt(privkey_or_seed, pubkey, ciphertext)
    print('\n\n%s\n' % plaintext)
