#!/usr/bin/env python3
"""
Merge multiple un-signed Transaction documents into one

... and remember kids, never roll your own crypto ;)
"""

import os
import sys
from operator import itemgetter

import unsigned_transaction

assumed_base = 0

def assert_sanity(transaction_documents, currency):
    for tx in transaction_documents:
        lines = tx.split('\n')
        assert lines[0] == 'Version: 10'
        assert lines[1] == 'Type: Transaction'
        assert lines[2] == 'Currency: %s' % currency
        assert lines[4] == 'Locktime: 0'
    return True

def merge_blockstamps(transaction_documents):
    bstamps = []
    for tx in transaction_documents:
        start, end = tx.index('Blockstamp: ')+12, tx.index('Locktime:')-1
        bstamps.append(tx[start:end])
    blockstamp = '%d-%s' % max([(int(bnum), bhash) for bnum, bhash in [
        bstamp.split('-') for bstamp in bstamps]])
    return blockstamp

def merge_issuers(transaction_documents, enforce_unique=False):
    issuers = []
    for tx in transaction_documents:
        start, end = tx.index('Issuers:\n')+9, tx.index('Inputs:\n')-1
        these_issuers = tx[start:end].split('\n')
        for issuer in these_issuers:
            if issuer not in issuers:
                issuers.append(issuer)
            elif enforce_unique:
                raise InputError('Duplicate issuer found: %s' % issuer)
    return issuers

def merge_inputs(transaction_documents):
    inputs = []
    for tx in transaction_documents:
        start, end = tx.index('Inputs:\n')+8, tx.index('Unlocks:\n')-1
        these_inputs = tx[start:end].split('\n')
        for input_ in these_inputs:
            base = int(input_.split(':')[1])
            if base != assumed_base:
                raise InputError('base %d is not assumed_base: %d' % (base, assumed_base))
            if input_ in inputs:
                raise InputError('Duplicate input found: %s' % input_)
            inputs.append(input_)
    return inputs

def merge_unlocks(transaction_documents, issuers, inputs):
    unlocks = []
    for tx in transaction_documents:
        these_issuers = merge_issuers([tx], enforce_unique=True)
        these_inputs = merge_inputs([tx])
        start, end = tx.index('Unlocks:\n')+9, tx.index('Outputs:\n')-1
        these_unlocks = tx[start:end].split('\n')
        for unlock in these_unlocks:
            in_ix, cond = unlock.split(':')
            input_ = these_inputs[int(in_ix)]
            assert cond.startswith('SIG(') and cond.endswith(')')
            iss_ix = int(cond[4:-1])
            issuer = these_issuers[iss_ix]
            unlocks.append([inputs.index(input_), issuers.index(issuer)])
    unlocks = sorted(unlocks, key=itemgetter(0,1))
    return ['%d:SIG(%s)' % (x, y) for x, y in unlocks]

def merge_outputs(transaction_documents):
    outputs = []
    for tx in transaction_documents:
        start, end = tx.index('Outputs:\n')+9, tx.index('Comment:')-1
        outputs += tx[start:end].split('\n')
    conditions = set([x.split(':')[2] for x in outputs])
    outputs_dict = {x: 0 for x in conditions}
    for output in outputs:
        amt, base, condition = output.split(':')
        amt, base = int(amt), int(base)
        if base != assumed_base:
            raise InputError('base:%d is not assumed_base:%d' % (base, assumed_base))
        outputs_dict[condition] += amt * pow(10, base)
    outputs = []
    for condition, amt in outputs_dict.items():
        outputs.append('%d:%d:%s' % (amt, base, condition))
    return outputs

def merge_comments(transaction_documents):
    comments = []
    for tx in transaction_documents:
        start = tx.index('Comment:')+8
        comment = tx[start:].strip()
        if comment and comment not in comments:
            comments.append(comment)
    return comments

def merge(transaction_documents, currency, hide_merge_order=True):
    assert_sanity(transaction_documents, currency)

    blockuid = merge_blockstamps(transaction_documents)
    locktime = 0
    issuers = merge_issuers(transaction_documents)
    inputs = merge_inputs(transaction_documents)
    outputs = merge_outputs(transaction_documents)
    comments = merge_comments(transaction_documents)
    if hide_merge_order:
        issuers = sorted(issuers)
        inputs = [':'.join(inp_) for inp_ in
            sorted([x.split(':') for x in inputs], key=itemgetter(2,3,4))
            ]
        outputs = [':'.join(out) for out in
            sorted([x.split(':') for x in outputs], key=itemgetter(2,0))
            ]
        comments = sorted(comments)
    unlocks = merge_unlocks(transaction_documents, issuers, inputs)
    comment = '; '.join(comments)

    return unsigned_transaction.getdoc(currency, blockuid, locktime,
        issuers, inputs, unlocks, outputs, comment
        )


if __name__ == '__main__':

    print('\nI need unsigned Transaction documents pasted one at a time.')

    currency = os.environ.get('DUNITER_CURRENCY', 'g1-test')

    docs = []
    while True:
        print('\nPaste a Transaction document, or nothing, then hit <Ctrl-D>')
        doc = sys.stdin.read()
        if not doc: break
        docs.append(doc)

    print('\n\n%s' % merge(docs, currency))
