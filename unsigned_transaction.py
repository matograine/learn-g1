#!/usr/bin/env python3
"""
Create an un-signed transaction document

... and remember kids, never roll your own crypto ;)
"""

import sys
import os

import requests

class InputError(Exception): pass

fmt_document = '''Version: 10
Type: Transaction
Currency: %s
Blockstamp: %s
Locktime: %d
Issuers:
%s
Inputs:
%s
Unlocks:
%s
Outputs:
%s
Comment:%s
'''

def validate_comment(comment):
    comment_alphabet = ''.join([
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 
        'abcdefghijklmnopqrstuvwxyz', 
        '0123456789', 
        ' -_:/;*[]()?!^+={}\<>%'
        ])
    invalid_chars = ''.join(set(
        [x for x in comment if x not in comment_alphabet]
        ))
    if invalid_chars:
        raise InputError('Char(s) "%s" from comment NOT in "%s"' % (
            invalid_chars, comment_alphabet))
    if len(comment) >255:
        raise InputError('Comment length must not exceed 255 characters')
    return True

def validate_compact_length(issuers, inputs, comment):
    compact_lines = 2 + (len(issuers) * 2) + (len(inputs) * 2) + (
        1 if comment else 0)
    if compact_lines > 100: 
        raise InputError('Compact-Transaction lines: %d exceed max: 100' 
            % compact_lines)
    return True

def validate_consumed_spent(inputs, outputs):
    consumed = sum([int(x.split(':')[0]) for x in inputs])
    spent = sum([int(x.split(':')[0]) for x in outputs])
    assert consumed == spent
    return True

def getdoc(currency, blockuid, locktime, 
        issuers, inputs, unlocks, outputs, comment=''):
    assert validate_compact_length(issuers, inputs, comment)
    assert validate_comment(comment)
    assert validate_consumed_spent(inputs, outputs)
    assert len(unlocks) == len(inputs)

    if comment: comment = ' ' + comment
    return fmt_document % (
        currency, blockuid, locktime, 
        '%s' % '\n'.join([x for x in issuers]),
        '%s' % '\n'.join([x for x in inputs]),
        '%s' % '\n'.join([x for x in unlocks]),
        '%s' % '\n'.join([x for x in outputs]),
        comment)


if __name__ == '__main__':

    print('\nI will be requesting BMA results from a duniter node.')

    currency = os.environ.get('DUNITER_CURRENCY', 'g1-test')

    node_uri = os.environ.get('DUNITER_BMA', None)
    default = 'http://127.0.0.1:20900'
    if not node_uri:
        print('\nEnter a duniter-bma URI (default: %s), then hit <enter>' 
            % default)
        node_uri = input()
        if not node_uri: node_uri = default

    print('\nRequesting %s/blockchain/current...' % node_uri)
    response = requests.get(node_uri + '/blockchain/current')
    json = response.json()
    latest = json['number']
    print('...current height is %d.' % latest)

    print('\nRequesting %s/blochain/block/%d...' % (node_uri, latest-6))
    response = requests.get(node_uri + '/blockchain/block/%d' % (latest-6))
    json = response.json()
    blockuid = '%d-%s' % (json['number'], json['hash'])
    print('...using %s.' % blockuid)

    print('\nPaste your base58 pubkey as Issuer, then hit <enter>')
    pubkey = input()

    print('\nRequesting %s/tx/sources/%s...' % (node_uri, pubkey))
    response = requests.get(node_uri + '/tx/sources/%s' % pubkey)
    json = response.json()

    if 'sources' in json: 
        balance = sum([x['amount'] for x in json['sources']])
        print('...%d sources found.  Balance: %.2f G1' % (
            len(json['sources']), balance/100))

        spends, spending = [], 0
        print('\nSpecify the pay-to outputs below, one entry per line.')
        print('Format each like: "Amount PubKey"; when done, hit <Ctrl-D>')
        payto_lines = sys.stdin.read().split('\n')
        for amt_payto in payto_lines:
            if not amt_payto: break
            amt, payto = amt_payto.split(' ')
            amt = float(amt)
            spending += round(amt*100)
            spends.append([round(amt*100), payto])
        print('Total amount being spent: %.2f G1' % (spending/100))
    else:
        print('...no results found!')

    print('\nType anything to select specific sources, or nothing, then hit <enter>')
    select_sources = input()
    if select_sources:
        for i, source in enumerate(json['sources']):
            print('%3d) %10.2f %s %s:%d' % (i, source['amount']/100,
                source['type'], source['identifier'], source['noffset']))
        print('\nEnter csv-list of indexes from above sources, then hit <enter>')
        source_indices = [int(x) for x in input().split(',')]
        sources = []
        for i in source_indices:
            sources.append(json['sources'][i])
    else:
        sources = json['sources']
            
    issuers = [pubkey]
    inputs = []
    unlocks = []
    consumed, base = 0, 0
    for i, source in enumerate(sources):
        consumed += source['amount']
        inputs.append('%d:%d:%s:%s:%d' % (
            source['amount'], source['base'], source['type'],
            source['identifier'], source['noffset']))
        conditions = source['conditions'].split(' ')
        unls = []
        for cond in conditions:
            if cond[:4] == "SIG(" and cond[-1:] == ')':
                unls.append('SIG(%d)' % issuers.index(cond[4:-1]))
            else:
                unls.append(cond)
        unlocks.append('%d:' % i + ' '.join([x for x in unls]))
        if not select_sources and consumed >= spending:
            break

    outputs = []
    for amt, payto in spends:
         outputs.append('%d:%d:SIG(%s)' % (amt, base, payto))
    if consumed > spending:
         outputs.append('%d:%d:SIG(%s)' % (consumed-spending, base, pubkey))

    print('\nEnter a comment below, or nothing, then hit <enter>')
    comment = input()
         
    print('\n\n%s' % getdoc(
        currency, blockuid, 0, issuers, inputs, unlocks, outputs, comment
        ))
