#!/usr/bin/env python3
"""
create a new wallet, 
    displays: secret credentials, public-key, private key and seed.
    by default: (w/o fileprefix) saves nothing to disk, 
    else saves keychain file as: authfile, PubSec, WIF or EWIF

... and remember kids, never roll your own crypto ;)
"""

import os
from hashlib import sha256, scrypt

from base58 import b58encode
from libnacl.sign import Signer, Verifier
from pyaes import AESModeOfOperationECB
from termcolor import colored

keychain_types = ('authfile', 'pubsec', 'wif', 'ewif')

class Wallet:
    def __init__(self, identifier, password, N=4096, r=16, p=1):
        seed = scrypt(password, salt=identifier, n=N, r=r, p=p, dklen=32)
        self._keys = Signer(seed)

    def signature(self, messageBytes):
        return self._keys.signature(messageBytes)

    def verifySignature(self, signature, messageBytes):
        assert Verifier(self._keys.vk.hex()).verify(signature + messageBytes)
        return True

    def save_authfile(self, fileprefix):
        filename = fileprefix + '.authfile'
        self._save_to_file(filename, self._keys.seed.hex())
        print("private seed file saved: %s" % filename)

    def save_pubsec(self, fileprefix):
        filename = fileprefix + '-PubSec.dunikey'
        self._save_to_file(filename,
            'Type: PubSec\nVersion: 1\npub: %s\nsec: %s' % (
            b58encode(self._keys.vk).decode('utf-8'),
            b58encode(self._keys.sk).decode('utf-8')))
        print('PubSec file saved: %s' % filename)

    def save_wif(self, fileprefix):
        filename = fileprefix + '-WIF.dunikey'
        v1_seed = b'\x01' + self._keys.seed
        checksum = sha256(sha256(v1_seed).digest()).digest()[:2]
        self._save_to_file(filename, 'Type: WIF\nVersion: 1\nData: %s' % (
            b58encode(v1_seed + checksum).decode('utf-8')))
        print('WIF file saved: %s' % filename)

    def save_ewif(self, fileprefix, password=b''):
        filename = fileprefix + '-EWIF.dunikey'
        salt = sha256(sha256(self._keys.vk).digest()).digest()[:4]
        scrypt_seed = scrypt(password, salt=salt, n=16384, r=8, p=8, dklen=64)
        xor_seeds = bytes([a ^ b for a, b in zip(
            self._keys.seed, scrypt_seed[:32])])
        aes = AESModeOfOperationECB(scrypt_seed[32:])
        ewif = b''.join([b'\x02', salt, 
            aes.encrypt(xor_seeds[:16]), 
            aes.encrypt(xor_seeds[16:])
            ])
        checksum = sha256(sha256(ewif).digest()).digest()[:2]
        self._save_to_file(filename, 'Type: EWIF\nVersion: 1\nData: %s' % (
             b58encode(ewif + checksum).decode('utf-8')))
        print('EWIF file saved: %s' % filename)

    def _save_to_file(self, filename, contents):
        with open(filename, 'w') as fp:
            fp.write(contents)
            os.chmod(filename, 0o600)


def main(identifier, password, 
        N, r, p, message, keychain=None, fileprefix=None, ewif_passwd=None):
    print("creating keychain with: id=%s and passwd=%s, N=%d, r=%d, p=%d" % (
          identifier, password, N, r, p))
    myWallet = Wallet(identifier, password, N, r, p)
    print("seed (base58):", b58encode(myWallet._keys.seed).decode('utf-8'))
    print("privkey (base58):", b58encode(myWallet._keys.sk).decode('utf-8'))
    print("pubkey (base58):", b58encode(myWallet._keys.vk).decode('utf-8'))
    print("can sign and verify:", 
        myWallet.verifySignature(myWallet.signature(msg), msg))
        
    if keychain and fileprefix:
        if keychain == 'authfile': myWallet.save_authfile(fileprefix)
        elif keychain == 'pubsec': myWallet.save_pubsec(fileprefix)
        elif keychain == 'wif': myWallet.save_wif(fileprefix)
        elif keychain == 'ewif': myWallet.save_ewif(fileprefix, ewif_passwd)
        else: raise Exception('Unexpected program error')

if __name__ == '__main__':

    warning = colored('Warning!', 'white', 'on_red', attrs=['blink', 'bold'])

    print('\n%s\nEnter scrypt salt (your secret-identifier), then hit <enter>'
        % warning)
    ident = bytes(input(), 'utf-8')

    print('\n%s\nEnter scrypt passphrase (your password), then hit <enter>'
        % warning)
    passwd = bytes(input(), 'utf-8')

    default = '4096,16,1'
    print('\nEnter csv scrypt-parameters (default: %s), then hit <enter>' 
        % default)
    scrypt_params = input()
    if not scrypt_params: scrypt_params = default
    N, r, p = [int(x) for x in scrypt_params.split(',')]

    print('\n%s\nSpecify a keychain type (%s) or nothing, then hit <enter>' 
        % (warning, ','.join([x for x in keychain_types])))
    keychain = input()

    ewif_passwd, fileprefix = None, None
    if keychain and keychain in keychain_types:
        if keychain == 'ewif':
            print('\nSpecify a keychain password, then hit <enter>')
            ewif_passwd = bytes(input(), 'utf-8')

        print('\nEnter a keychain filename prefix, then hit <enter>')
        fileprefix = input()

    msg = b'this is a message'

    main(ident, passwd, N, r, p, msg, keychain, fileprefix, ewif_passwd)
