#!/usr/bin/env python3

from hashlib import scrypt
from base58 import b58encode
from libnacl.sign import Signer

pubkey_lines = '''THE PUBKEYS YOU WANT TO CRACK BELOW, ONE PER LINE
'''

salt_lines = '''THE POSSIBLE SECRET-IDENTIFIERS BELOW, ONE PER LINE
'''
pwd_lines = '''THE POSSIBLE PASSWORDS BELOW, ONE PER LINE
'''

pubkeys = pubkey_lines.split('\n')[1:]
salts = salt_lines.split('\n')[1:]
pwds = pwd_lines.split('\n')[1:]

successes = []
def attempt(salt, pwd):
    print(salt, pwd)
    seed = scrypt(pwd, salt=salt, n=4096, r=16, p=1, dklen=32)
    pubkey = b58encode(Signer(seed).vk).decode('utf-8')
    if pubkey in pubkeys:
        print('pwd:%s, salt:%s, pubkey: %s\n' % (pwd, salt, pubkey))
        if [salt, pwd] not in successes:
            successes.append([salt, pwd])

for salt in [bytes(x, 'utf-8') for x in salts]:
    for pwd in [bytes(x, 'utf-8') for x in pwds]:
        attempt(salt,pwd) and attempt(pwd,salt)
        for i in range(len(salt)+1):
            s = salt[i:]
            attempt(s,pwd) and attempt(pwd,s)
            s = salt[:-i]
            attempt(s,pwd) and attempt(pwd,s)
        for i in range(len(pwd)+1):
            p = pwd[i:]
            attempt(salt,p) and attempt(p,salt)
            p = pwd[:-i]
            attempt(salt,p) and attempt(p,salt)

for salt, pwd in successes:
    attempt(salt, pwd)
