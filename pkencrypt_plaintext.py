#!/usr/bin/env python3
"""
Encrypt "plaintext" with sender "private-key" and recipient "public-key"

...and remember kids, never roll your own crypto ;)
"""

import sys
import base64

import base58
from libnacl import crypto_sign_ed25519_sk_to_curve25519 as private_sign2crypt
from libnacl import crypto_sign_ed25519_pk_to_curve25519 as public_sign2crypt
from libnacl.public import SecretKey, PublicKey, Box
from libnacl.sign import Signer, Verifier
from termcolor import colored

def encrypt(b58from_privkey_or_seed, b58to_pubkey, plaintext, nonce24b=None):
    seed = base58.b58decode(b58from_privkey_or_seed)[:32]
    signer = Signer(seed)
    sk = SecretKey(private_sign2crypt(signer.sk))
    vk = Verifier(base58.b58decode(b58to_pubkey).hex())
    pk = PublicKey(public_sign2crypt(vk.vk))
    box = Box(sk.sk, pk.pk)
    ciphertext = box.encrypt(bytes(plaintext, 'utf-8'), nonce24b)
    return base64.b64encode(ciphertext).decode('utf-8')
    

if __name__ == '__main__':

    warning = colored('Warning!', 'white', 'on_red', attrs=['blink', 'bold'])
    print('\n%s\nPaste your base58 private-key or seed, then hit <enter>'
        % warning)
    privkey_or_seed = input()

    print("\nPaste the recipient's base58 public-key below, then hit <enter>") 
    pubkey = input()

    print('\nPaste a 24-byte base64 nonce (or nothing) below, then hit <enter>') 
    nonce = input()
    if nonce: nonce = base64.b64decode(nonce)
    else: nonce = None

    print('\nPaste the plaintext message below, then hit <Ctrl-D>')
    plaintext = sys.stdin.read()

    ciphertext = encrypt(privkey_or_seed, pubkey, plaintext, nonce)
    print('\n\n%s\n\nEncrypted! The above base64 ciphertext may be decrypted, using your pubkey, by the owner of public-key: %s' % (
        ciphertext, pubkey))
