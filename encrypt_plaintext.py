#!/usr/bin/env python3
"""
Encrypt "plaintext" for a recipient's "public-key"

...and remember kids, never roll your own crypto ;)
"""

import sys
import base64

import base58
from libnacl import crypto_sign_ed25519_pk_to_curve25519 as public_sign2crypt
from libnacl.sign import Verifier
from libnacl.public import PublicKey
from libnacl.sealed import SealedBox

def encrypt(b58to_pubkey, plaintext):
    vk = Verifier(base58.b58decode(b58to_pubkey).hex())
    pk = PublicKey(public_sign2crypt(vk.vk))
    box = SealedBox(pk)
    ciphertext = box.encrypt(bytes(plaintext, 'utf-8'))
    return base64.b64encode(ciphertext).decode('utf-8')

if __name__ == '__main__':

    print("\nPaste the recipient's base58 public-key below, then hit <enter>") 
    pubkey = input()

    print('\nPaste the plaintext message below, then hit <Ctrl-D>')
    plaintext = sys.stdin.read()

    print('\n\n%s\n\nEncrypted! The above base64 ciphertext may be decrypted by the owner of public-key: %s' % (
        encrypt(pubkey, plaintext), pubkey))
