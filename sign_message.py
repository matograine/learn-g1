#!/usr/bin/env python3
"""
Sign a message with a "private-key" 

... and remember kids, never roll your own crypto ;)
"""

import sys
import base64

from libnacl.sign import Signer
from base58 import b58encode, b58decode
from termcolor import colored

def get_signature(b58priv_key_or_seed, message):
    seed = b58decode(privkey_or_seed)[:32]
    signer = Signer(seed)
    pubkey = b58encode(signer.vk)
    message = bytes(message, 'utf-8')
    signature = base64.b64encode(signer.signature(message)).decode('utf-8')
    return signature, pubkey.decode('utf-8')


if __name__ == '__main__':

    warning = colored('Warning!', 'white', 'on_red', attrs=['blink', 'bold'])
    print('\n%s\nPaste your base58 private-key or seed, then hit <enter>'
        % warning) 
    privkey_or_seed = input()

    print('\nPaste the message below, then hit <Ctrl-D>')
    msg = sys.stdin.read()

    sig, pubkey = get_signature(privkey_or_seed, msg)
    print('\n\n%s\nThe above signature for this message can be verified for pubkey %s' % ( 
        sig, pubkey))
