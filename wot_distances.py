#!/usr/bin/env python3
"""
directed WoT walk backwards from member, noting sentinel distances

... and remember kids, never roll your own crypto ;)
"""

import json

from jsonize_wot import wot_load, wot_summary, wot_members, wot_sentinels

stepMax = 5      # defined in genesis, bma /blockchain/parameters
xPercent = 0.8   # defined in genesis, bma /blockchain/parameters
sentinel_if = 5  # in reality: ceil(pow(num_members, 1/stepMax))

if __name__ == '__main__':

    class InputError(Exception): pass

    wot = wot_load()
    wot_summary(wot)

    print('\nPaste a base58 public-key or UniqueID, then hit <enter>')
    pubkey_or_uid = input()
    if pubkey_or_uid in wot:
        thispubkey = pubkey_or_uid
    else:
        thispubkey = [k for k,v in wot.items() if v['uid'] == pubkey_or_uid][0]
    print('\nCalculating distances backwards from %s' % thispubkey)

    sentinels = wot_sentinels(wot)
    members = wot_members(wot)

    i, needed, sentinel_distances = 0, [thispubkey], {}
    visited, non_sentinels, non_members = [], [], []
    while needed:
        sentinel_distances[i], new = [], []
        for pubkey in needed:
            this = wot[pubkey]
            visited.append(pubkey)
            if pubkey not in members: non_members.append(pubkey)
            if pubkey not in sentinels: non_sentinels.append(pubkey)
            else: sentinel_distances[i].append(pubkey)
            certs = this['received']
            new += [x for x in certs if x not in new]
        needed = [x for x in new if x not in visited]
        i += 1

    visited_sentinels, sentinels_in_reach = 0, 0
    for lvl in sorted(sentinel_distances):
        visited_sentinels += len(sentinel_distances[lvl])
        if len(sentinel_distances[lvl]):
            print('steps: %d, new sentinels: %d, total: %d' % (
                lvl, len(sentinel_distances[lvl]), visited_sentinels))
        if lvl == stepMax-1:
            next_sent_reaches = visited_sentinels
        elif lvl == stepMax: 
            sentinels_in_reach = visited_sentinels

    ignored = 1 if thispubkey in sentinels else 0
    print('%s is sentinel: %s, member: %s' % (wot[thispubkey]['uid'], 
        thispubkey in sentinels, thispubkey in members))
    print('...uidblk: %d, eldest: %d, latest: %d' % (
        wot[thispubkey]['uidblk'], wot[thispubkey]['eldest'], 
        wot[thispubkey]['latest']))
    print('visited: %d' % len(visited))
    print('non-members: %d' % len(non_members))
    print('non-sentinels: %d' % len(non_sentinels))
    print('isolated members: %d' % len(
        [x for x in members if x not in visited]))
    print('isolated sentinels: %d' % len(
        [x for x in sentinels if x not in visited]))
    print('sentinels reached after %d steps: %d/%d %.2f%%' % (
        stepMax, sentinels_in_reach-ignored, len(sentinels)-ignored,
        ((sentinels_in_reach-ignored) / (len(sentinels)-ignored)) * 100
        ))
    print('sentinels reached via cert sent: %d/%d %.2f%% quality: %.2f' % (
        next_sent_reaches, len(sentinels),
        (next_sent_reaches / len(sentinels)) * 100, 
        (next_sent_reaches / len(sentinels)) / xPercent
        ))
