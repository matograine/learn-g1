#!/usr/bin/env python3
"""
Verify the "signature" of a "message" with a "public-key"

...and remember kids, never roll your own crypto ;)
"""

import sys
import base64

from base58 import b58decode
from libnacl.sign import Verifier

def verify(b58pubkey, b64signature, message):
    verifier = Verifier(b58decode(b58pubkey).hex())
    signature = base64.b64decode(b64signature)
    return verifier.verify(signature + bytes(message, 'utf-8'))

if __name__ == '__main__':
    print("\nPaste the signer's base58 public-key below, then hit <enter>") 
    pubkey = input()

    print('\nPaste the base64 signature of the message below, then hit <enter>')
    signature = input()

    print('\nPaste the message below, then hit <Ctrl-D>')
    msg = sys.stdin.read()

    verified = verify(pubkey, signature, msg)
    if verified:
        print('\nMessage Signature Verified for %s' % pubkey)
    else:
        print('\nINVALID Signature!')
